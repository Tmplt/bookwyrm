📜 Bookwyrm
---
bookwyrm is work-in-progress TUI-based program written in modern C++ which, given some input data,
will search for matching ebooks and academic papers on various sources.
During runtime, all found items will be presented in a menu,
where you can choose which items you want to download.

When implemented, an item can be viewed for details, which will be fetched from some database (unless the source itself holds enough data), such as the Open Library or WorldCat.
A screen holding logs from worker threads is available by pressing TAB. All unread logs are printed to std{out,err} upon program termination.

For example, one might run bookwyrm as follows:

    bookwyrm --author "Naomi Novik" --series Temeraire --year >=2015 .

Here is a (possible outdated) video of bookwyrm's TUI in action:
[![asciicast](https://asciinema.org/a/dGYzT0k95jcoyHxEqS9oANAtm.png)](https://asciinema.org/a/dGYzT0k95jcoyHxEqS9oANAtm)
In the log screen, issue #46 can be observed.

Sources are written as scripts which run in their own worker threads.
Some scripts will be available upstream, but you should also be able to write your own into `~/.config/bookwyrm/sources/`. Currently, only Python scripts are supported, but LUA is being considered.
A script may need data from the user (e.g. login credentials); this can be written into `~/.config/bookwyrm/config.yaml`, when implemented.

Aside from a C++17-compliant compiler and CMake, bookwyrm also depends on a few libraries:
* fmt,        for a few print-outs and since spdlog depends on it;
* **ncurses**,        for the TUI;
* **pybind11**,   for interfacing with Python, and
* **fuzzywuzzy**, for fuzzily matching found items with what's wanted.

All libraries that do not use a bold font are non-essential and may be subject to removal later in development. (Almost) all dependencies are submoduled in `lib/`.
The only external C++ dependency is ncurses.

Furthermore, the Python modules need the following packages:
* **furl**,     for parsing and modifying URLs;
* **requests**,     for sane HTTP requests, and
* isbnlib,      for validating ISBNs.

If you're not using Nix(OS), these can be installed via `pip3 install [--user] -r requirements.txt`.

As of v0.5.0, bookwyrm is in a quasi-usable state, but nothing is yet set in stone;
a major release is some ways off.
A plugin for Library Genesis is available upstream.

If you have any insights, questions, or wish to contribute,
create a PR/issue on Github at <https://github.com/Tmplt/bookwyrm>.
You can also contact me via email at `echo "dG1wbHRAZHJhZ29ucy5yb2Nrcwo=" | base64 -d`.

Building instructions
---
```
$ git clone ...
$ git submodule update --init --recursive
$ mkdir build && cd build
$ cmake .. && make
$ src/bookwyrm OPTION [OPTION]... PATH
```

If you're using Nix(OS), just clone the repository and run `nix build`.
If you want to enter a build environment, evaluate the expression in `shell.nix` first (append `nix-shell` after cloning the repository).

If you're using Arch Linux, bookwyrm is available on the AUR as `bookwyrm-git`.
The `PKGBUILD` contain some configuration fixes vis-a-vis a Nix environment.
